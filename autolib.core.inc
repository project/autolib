<?php

/**
 * @file
 * Core functionality of the autolib module that does not involve hooks.
 */

use Drupal\Core\Extension\Extension;

/**
 * @param \Drupal\Core\Extension\Extension $extension
 *  The extension to try to get libraries for.
 * @param array $libraries
 *   The array to which to append the library definitions to.
 */
function _addLibrariesFromExtension(Extension $extension, &$libraries) {
  $extensionPath = $extension->getPath();
  $components_path = _getComponentsPathForExtension($extension);
  $files = _recursiveDirectorySearch($components_path, '/^.*\.(css|js)$/');

  foreach ($files as $file) {
    $fileInfo = pathinfo($file);
    $relative_file_path = str_replace("$extensionPath/", '', $file);
    $fileInfo['extension_relative'] = $relative_file_path;

    $libraryName = _getLibraryNameForFileInExtension($extension, $fileInfo);
    $entry = _getLibraryEntryForFile($fileInfo);
    $libraries = array_merge_recursive($libraries, [$libraryName => $entry]);
  }
}

/**
 * @param $fileInfo
 *
 * @return array
 */
function _getLibraryEntryForFile($fileInfo) {
  $asset = [];
  // The options to set for each added CSS and JS file.
  $css_options = ['minified' => TRUE];
  $css_weight_group = 'component';
  $js_options = ['minified' => TRUE];

  $relative_file_path = $fileInfo['extension_relative'];

  // The file is appended to allow for multiple files per library.
  // The keys ['css']['component'] refer to the CSS weight group.
  // @see https://www.drupal.org/node/2274843#library
  switch ($fileInfo['extension']) {
    case 'css':
      $asset['css'][$css_weight_group][$relative_file_path] = $css_options;
      break;
    case 'js':
      $asset['js'][$relative_file_path] = $js_options;
      break;
  }
  return $asset;
}

/**
 * @param \Drupal\Core\Extension\Extension $extension
 * @param $fileInfo
 *
 * @return string
 */
function _getLibraryNameForFileInExtension(Extension $extension, $fileInfo) {
  $components_path = _getComponentsPathForExtension($extension);
  $identifier = str_replace("$components_path", '', $fileInfo['dirname']);

  // Configurable variables.
  $prefix = '';
  $subfolderSeparator = '.';
  $useFilenameAsKey = FALSE;

  // If the prefix includes the separator, remove it.
  $name_prefix = trim($prefix, $subfolderSeparator);
  if (!empty($name_prefix)) {
    // Add back the separator if the prefix is not empty.
    $name_prefix .= $subfolderSeparator;
  }

  $key = str_replace('/', $subfolderSeparator, $identifier);

  if (empty($key)) {
    $useFilenameAsKey = TRUE;
  }

  if ($useFilenameAsKey) {
    $key = $fileInfo['filename'];
  }

  $key = trim($key, $subfolderSeparator);

  return "{$name_prefix}{$key}";
}

/**
 * Returns the (absolute) path to scan for files for the given $extension.
 *
 * The returned path will not contain a trailing slash.
 *
 * @param \Drupal\Core\Extension\Extension $extension
 *   The extension (module or theme) to get the path for.
 *
 * @return string
 *   The (absolute) path of the components directory for the given $extension.
 */
function _getComponentsPathForExtension(Extension $extension) {
  $extensionPath = $extension->getPath();
  $relative_components_path = 'dist';

  return trim("$extensionPath/$relative_components_path", '/');
}

/**
 * Recursively search a given $directory for files matching a given $pattern.
 *
 * @param string $directory
 *   The directory to scan recursively for files.
 * @param string $pattern
 *   The pattern that will match the desired files.
 *
 * @return array
 *   An array of files in $directory that match $pattern.
 */
function _recursiveDirectorySearch($directory, $pattern) {
  $fileList = [];
  $matchingFiles = [];

  try {
    $directoryIterator = new RecursiveDirectoryIterator($directory);
    $iteratorIterator = new RecursiveIteratorIterator($directoryIterator);
    $matchingFiles = new RegexIterator($iteratorIterator, $pattern, RegexIterator::ALL_MATCHES);
  } catch (\UnexpectedValueException $e) {
    \Drupal::logger('autolib')->warning('Directory @dir does not exist.', [
      '@dir' => $directory
    ]);
  }

  foreach ($matchingFiles as $file) {
    $fileList = array_merge($fileList, $file[0]);
  }

  return $fileList;
}